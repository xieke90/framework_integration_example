### framework_integration_example

常用框架整合案例 - 开箱即用

### 作者及博客

QQ：949118693（邪客）

Java技术交流群：780509097

![输入图片说明](https://gitee.com/uploads/images/2018/0616/091012_2e93400f_583593.png "Java技术交流群.png")

博客：http://xieke90.iteye.com/

### 项目结构说明

**1. springbootdemo：maven + spring boot + spring data jpa + thymeleaf整合框架案例** 

**2. springbootmybatisdemo：maven + spring boot + mybatis整合框架案例** 

**3.  ssm-freemarker-demo：maven + spring + spring mvc + mybatis + freemarker整合框架案例** 


### 后续持续更新...

